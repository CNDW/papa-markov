# Papa Markov
## The dad joke generator
A dad joke generator using a Markov Chain algorithm and data sourced from http://icanhazdadjoke.com

## Requirements
- node 12.1.0 (defined in the .nvmrc file)
- [yarn](https://yarnpkg.com/en/docs/install)
- docker-compose

## To run the app
1) Clone this repository
1) Run `yarn` from the repository root directory to install dependencies
1) Run `yarn start:all` from the repository to run the application
1) navigate to `http://localhost:3000` in your browser

## About
- The markov chain logic is written in typescript inside of `/packages/generator/src/api/jokes`
- The frontend is written in react and typescript
- The backend is a simple koa server that utilizes redis for caching and persistence
