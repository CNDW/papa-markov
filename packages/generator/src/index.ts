import createApp from './api/app';

async function run() {
    const app = await createApp();

    app.listen({ port: '3000'}, () => {
        console.log('Server listening at http://localhost:3000');
    });
}

run();
