import { META_COUNT_KEY } from './constants';

export interface Joke {
    id: string;
    joke: string;
    status: number;
}

export interface Word {
    [META_COUNT_KEY]: string;
    [key: string]: string;
}
