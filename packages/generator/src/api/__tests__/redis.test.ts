import { redis } from '../redis';

describe('redis client', () => {
    it('should have a working connection', async () => {
        const pong = await redis.ping();
        expect(pong).toEqual('PONG');
    });
});
