import { generateJoke } from '../jokes/generator';
import { learnJoke } from '../jokes/joke';
import { jokes } from '../__fixtures__/jokes';

describe('Joke generator', () => {
    beforeAll(async () => {
        await Promise.all(jokes.map(learnJoke));
    });

    it('should generate a joke', async () => {
        const newJoke = await generateJoke();

        expect(newJoke.split(' ').length).toBeGreaterThan(1);
    });
});
