import nock from 'nock';

import createTestApp, { TestApp } from '../__mocks__/test-app';

import { learnJoke } from '../jokes/joke';
import { jokes } from '../__fixtures__/jokes';
import { API_HOST } from '../constants';


describe('api', () => {
    let app = null as unknown as TestApp;
    beforeAll(async () => {
        app = await createTestApp();

        await Promise.all(jokes.map(learnJoke));
    });

    describe('GET /dad-joke', () => {
        it('should return a 200', async () => {
            await app.request('/dad-joke')
                .expect(200);
        });

        it('should return a joke', async () => {
            const response = await app.request('/dad-joke');

            expect(response.body).toEqual(expect.objectContaining({
                joke: expect.any(String)
            }));
        });

        it('should include overall stats in the payload', async () => {
            const response = await app.request('/dad-joke');

            expect(response.body).toEqual(expect.objectContaining({
                jokesRead: expect.any(Number),
                goodJokesCount: expect.any(Number),
                badJokesCount: expect.any(Number)
            }));
        });
    });

    describe('POST /dad-joke', () => {
        it('should return a 200 with an error if no joke is given to learn from', async () => {
            await app.request('/dad-joke', {method: 'post'})
                .send({})
                .expect(400);
        });

        it('should store a bad joke and return a new one', async () => {
            const response = await app.request('/dad-joke', {method: 'post'})
                .send({ bad: 'this is a bad joke' })
                .expect(202);

            expect(response.body).toEqual(expect.objectContaining({
                joke: expect.any(String)
            }));
        });

        it('should store a good joke and return a new one', async () => {
            const response = await app.request('/dad-joke', {method: 'post'})
                .send({ good: 'this is a good joke' })
                .expect(202);

            expect(response.body).toEqual(expect.objectContaining({
                joke: expect.any(String)
            }));
        });

        it('should include overall stats in the payload', async () => {
            const response = await app.request('/dad-joke', {method: 'post'})
                .send({ bad: 'this is a bad joke' })
                .expect(202);

            expect(response.body).toEqual(expect.objectContaining({
                jokesRead: expect.any(Number),
                goodJokesCount: expect.any(Number),
                badJokesCount: expect.any(Number)
            }));
        });
    });

    describe('POST /train', () => {
        it('should train on jokes from icanhazdadjoke.com for the number of times count is called', async () => {
            nock(API_HOST).get('/').reply(200, {
                id: 'a joke id', joke: 'this is a joke', status: 200
            });

            const response = await app.request('/train', {method: 'post'})
                .send({ times: 1 })
                .expect(202);

            expect(response.body).toEqual(expect.objectContaining({
                jokesRead: expect.any(Number),
                goodJokesCount: expect.any(Number),
                badJokesCount: expect.any(Number)
            }));
        });
    });
});
