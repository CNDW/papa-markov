import { META_COUNT_KEY } from '../constants';
import { selectNextWord } from '../jokes/generator';

describe('select-next-word', () => {
    const word = {
        [META_COUNT_KEY]: '20',
        a: '5',
        b: '1',
        c: '4',
        d: '10'
    };
    it('should randomly select a approximately 25% of the time', () => {
        let selectionCount = 0;
        for (let i = 0;i < 10000; i++) {
            const result = selectNextWord(word);
            if (result === 'a') {
                selectionCount++;
            }
        }

        const variance = Math.abs((selectionCount / 10000) - 0.25);
        expect(variance).toBeLessThan(0.01);
    });


    it('should randomly select b approximately 5% of the time', () => {
        let selectionCount = 0;
        for (let i = 0;i < 10000; i++) {
            const result = selectNextWord(word);
            if (result === 'b') {
                selectionCount++;
            }
        }

        const variance = Math.abs((selectionCount / 10000) - 0.05);
        expect(variance).toBeLessThan(0.01);
    });


    it('should randomly select c approximately 20% of the time', () => {
        let selectionCount = 0;
        for (let i = 0;i < 10000; i++) {
            const result = selectNextWord(word);
            if (result === 'c') {
                selectionCount++;
            }
        }

        const variance = Math.abs((selectionCount / 10000) - 0.2);
        expect(variance).toBeLessThan(0.01);
    });


    it('should randomly select d approximately 50% of the time', () => {
        let selectionCount = 0;
        for (let i = 0;i < 10000; i++) {
            const result = selectNextWord(word);
            if (result === 'd') {
                selectionCount++;
            }
        }

        const variance = Math.abs((selectionCount / 10000) - 0.5);
        expect(variance).toBeLessThan(0.01);
    });
});
