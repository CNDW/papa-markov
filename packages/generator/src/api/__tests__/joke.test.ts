import nock from 'nock';
import uuid from 'uuid/v4';

import { getRandomJoke, getStoredJoke, parseJoke } from '../jokes/joke';
import { API_HOST } from '../constants';
import { Joke } from '../types';


describe('getRandomJoke', () => {
    it('should retrieve a random joke from the api', async () => {
        const jokeId = uuid();
        nock(API_HOST).get('/').reply(200, {
            id: jokeId, joke: 'this is a joke', status: 200
        });

        const joke = await getRandomJoke();

        expect(joke.id).toEqual(jokeId);
    });

    it('should store a retrieved joke in redis', async () => {
        const jokeId = uuid();
        nock(API_HOST).get('/').reply(200, {
            id: jokeId, joke: 'this is a joke', status: 200
        });

        await getRandomJoke();
        const joke = await getStoredJoke(jokeId) as Joke;

        expect(joke.id).toEqual(jokeId);
    });
});

describe('parseJoke', () => {
    it('should split a joke into it`s respective parts', async () => {
        const result = parseJoke('this is a joke.');

        expect(result).toEqual(['this', 'is', 'a', 'joke', '.']);
    });

    it('should handle punctuation appropriately', async () => {
        const result = parseJoke('this, isn\'t a good; joke.');

        expect(result).toEqual(['this', ',', 'isn\'t', 'a', 'good', ';', 'joke', '.']);
    });
});
