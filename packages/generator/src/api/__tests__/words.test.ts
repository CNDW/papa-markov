import { storeWords, wordKeyFor } from '../jokes/words';
import { redis } from '../redis';
import { END, META_COUNT_KEY } from '../constants';

describe('storeWords', () => {
    it('should update the words in redis for a given chain example', async () => {
        await storeWords(['a']);
        const data = await redis.hgetall(wordKeyFor('a'));
        expect(Number(data[END])).toBeGreaterThanOrEqual(1);
    });

    it('should include a count of the number of times a word has been seen in overall sample', async () => {
        await storeWords(['a']);
        const data = await redis.hgetall(wordKeyFor('a'));
        expect(Number(data[META_COUNT_KEY])).toBeGreaterThanOrEqual(1);
    });
});
