import supertest from 'supertest';
import Koa from 'koa';
import createApp from '../app';

interface AppRequestOptions {
    method?: string;
}


export interface TestApp {
    app: Koa<any, {}>;
    request: (url: string, opts?: AppRequestOptions) => any;
}

export default async function createTestApp() {
    const app = await createApp();
    const appRequest = (url: string, opts: AppRequestOptions = {}) => {
        const request = supertest(app.callback())[opts.method || 'get'](url);

        const _assertStatus = request._assertStatus;
        request._assertStatus = ( status: any, res: any, ...rest: any[] ) => {
            const result = _assertStatus.call(request, status, res, ...rest);
            if (result as any instanceof Error) {
                const err = new Error(`${result.message}\n  ${res.text.replace('\n', '\n  ')}`) as any;
                err.expected = result.expected;
                err.actual = result.actual;
                err.showDiff = result.showDiff;
                return err;
            }
            return result;
        };

        return request;
    };

    return {app, request: appRequest} as TestApp;
}
