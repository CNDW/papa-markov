import { redis } from '../redis';

afterAll(async () => {
    redis.quit();
});
