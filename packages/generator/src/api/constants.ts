export const API_HOST = 'https://icanhazdadjoke.com';
export const START = '$$START$$';
export const END = '$$END$$';

export const JOKES_KEY = '$$jokes$$';
export const WORDS_KEY = '$$words$$';
export const META_COUNT_KEY = '$$meta-count$$';

export const JOKES_READ_KEY = '$$jokes-read$$';
export const GOOD_JOKES_KEY = '$$good-jokes$$';
export const BAD_JOKES_KEY = '$$bad-jokes$$';
