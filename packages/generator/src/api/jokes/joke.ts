import axios, { AxiosResponse } from 'axios';
import { redis } from '../redis';
import { API_HOST, JOKES_KEY, JOKES_READ_KEY, GOOD_JOKES_KEY, BAD_JOKES_KEY } from '../constants';
import { Joke } from '../types';
import { storeWords } from './words';

const client = axios.create({
    headers: {
        'Accept': 'application/json'
    }
});

export const getRandomJoke = async () => {
    const { data } = await client.get(API_HOST) as AxiosResponse<Joke>;

    await learnJoke(data);

    return data;
};

export const learnJoke = async (data: Joke) => {
    const stored = await getStoredJoke(data.id);
    if (!stored) {
        const jokeParts = parseJoke(data.joke);
        await Promise.all([
            storeJoke(data),
            storeWords(jokeParts),
            redis.incr(JOKES_READ_KEY)
        ]);
    }

    return data;
};

export const reinforceJoke = async (jokeString: string) => {
    const jokeParts = parseJoke(jokeString);
    await Promise.all([
        storeWords(jokeParts),
        redis.incr(JOKES_READ_KEY),
        redis.hincrby(GOOD_JOKES_KEY, jokeString, 1)
    ]);
};

export const discourageJoke = async (jokeString: string) => {
    const jokeParts = parseJoke(jokeString);
    await Promise.all([
        storeWords(jokeParts, false),
        redis.incr(JOKES_READ_KEY),
        redis.hincrby(BAD_JOKES_KEY, jokeString, 1)
    ]);
};

export const getStoredJoke = async (jokeId: string) => {
    const rawJoke = await redis.hget(JOKES_KEY, jokeId);

    if (!rawJoke) {
        return null;
    }

    try {
        return JSON.parse(rawJoke) as Joke;
    } catch (err) {
        console.error('could not parse joke', err);
        return null;
    }
};

export const storeJoke = async (joke: Joke) => redis.hset(JOKES_KEY, joke.id, JSON.stringify(joke));

export const parseJoke = (jokeString: string) => {
    const parts = jokeString.toLowerCase().match(/[,.!?;:]|\b[a-z']+\b/ig);
    if (!parts) {
        console.error(`could not parse joke '${jokeString}'`);
        return [];
    }
    return parts as string[];
};
