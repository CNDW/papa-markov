import { redis } from '../redis';
import { END, START, META_COUNT_KEY } from '../constants';
import { wordKeyFor } from './words';
import { Word } from '../types';

export function selectNextWord(word: Word) {
    let weight = Math.floor(Math.random() * Number(word[META_COUNT_KEY])) + 1;
    for (let key in word) {
        if (key === META_COUNT_KEY) continue;
        weight -= Number(word[key]);
        if (weight <= 0) {
            return key;
        }
    }

    console.log('unable to select next word, falling back to end');
    return END;
}

const isPunctuation = /^[.,:!?]/i;
const isSentanceEnd = /^[.:!?]/i;
const capitalize = (str: string) => str[0].toUpperCase() + str.slice(1);

export function formatText(currentWord: string, nextWord: string) {
    if (nextWord === END) {
        return '';
    }

    if (currentWord === START) {
        nextWord = capitalize(nextWord);
    } else if (isSentanceEnd.test(currentWord)) {
        nextWord = capitalize(nextWord);
    }

    if (nextWord === 'i' || nextWord === 'i\'m') {
        nextWord = capitalize(nextWord);
    }

    return `${isPunctuation.test(nextWord) ? '' : ' '}${nextWord}`;
}

export async function generateJoke() {
    const chain = [];
    let nextKey;
    let current = await redis.hgetall(wordKeyFor(START));
    let currentKey = START;

    do {
        nextKey = selectNextWord(current);
        const formattedText = formatText(currentKey, nextKey);
        current = await redis.hgetall(wordKeyFor(nextKey));
        currentKey = nextKey;
        if (formattedText) {
            chain.push(formattedText);
        }
    } while (nextKey !== END);

    return chain.join('').trim();
}
