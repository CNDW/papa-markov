import { redis } from '../redis';
import { START, END, WORDS_KEY, META_COUNT_KEY } from '../constants';

export const wordKeyFor = (part: string) => `${WORDS_KEY}${part}`;
export const storeWords = async (parts: string[], reinforce = true) => {
    const incrCount = reinforce ? 1 : -1;
    const pipeline = redis.pipeline();
    [START, ...parts]
        .map((part, idx, arry) => ({ part, next: arry[idx + 1] || END }))
        .forEach((item) => {
            const wordKey = wordKeyFor(item.part);
            pipeline.hincrby(wordKey, item.next, incrCount);
            pipeline.hincrby(wordKey, META_COUNT_KEY, incrCount);
        });

    return pipeline.exec();
};
