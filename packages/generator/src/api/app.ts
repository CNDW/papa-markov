import Koa from 'koa';
import cors from 'koa2-cors';
import Router from 'koa-router';
import bodyparser from 'koa-bodyparser';
import staticServe from 'koa-static';
import path from 'path';

import { generateJoke } from './jokes/generator';
import { reinforceJoke, discourageJoke, getRandomJoke } from './jokes/joke';
import { redis } from './redis';
import { JOKES_READ_KEY, GOOD_JOKES_KEY, BAD_JOKES_KEY } from './constants';

async function getStats() {
    const pipeline = redis.pipeline();
    pipeline.get(JOKES_READ_KEY);
    pipeline.hkeys(GOOD_JOKES_KEY);
    pipeline.hkeys(BAD_JOKES_KEY);
    const [jokesReadResp, goodJokesResp, badJokesResp] = await pipeline.exec();
    const [,jokesRead] = jokesReadResp;
    const [,goodJokes] = goodJokesResp;
    const [,badJokes] = badJokesResp;
    return {
        jokesRead: Number(jokesRead),
        goodJokesCount: goodJokes.length,
        badJokesCount: badJokes.length
    };
}

export default async function createApp() {
    const app = new Koa();
    app.use(cors());
    app.use(bodyparser());

    app.use(staticServe(path.resolve(__dirname, '..', '..', '..', 'webapp', 'static')));

    const router = new Router();


    router.get('/dad-joke', async (ctx) => {
        ctx.status = 200;
        const joke = await generateJoke();
        const stats = await getStats();
        ctx.body = { joke, ...stats };
    });

    router.post('/dad-joke', async (ctx) => {
        const { good, bad } = ctx.request.body;
        if (!good && !bad) {
            ctx.status = 400;
            ctx.body = { error: 'must include a `good` joke or a `bad` joke to learn from'};
            return;
        }

        if (good) {
            await reinforceJoke(good);
        } else if (bad) {
            await discourageJoke(bad);
        }

        ctx.status = 202;
        const joke = await generateJoke();
        const stats = await getStats();
        ctx.body = { joke, ...stats };
    });

    router.post('/train', async (ctx) => {
        let { count } = ctx.request.body;
        count = count || 1;
        const trainings = [];
        for (let idx = 0;idx < count;idx++) {
            trainings.push(getRandomJoke());
        }
        await Promise.all(trainings);
        ctx.status = 202;
        ctx.body = await getStats();
    });

    router.post('/restart', async (ctx) => {
        await redis.flushdb();
        const stats = await getStats();
        ctx.status = 202;
        ctx.body = stats;
    });

    app.use(router.routes());
    app.use(router.allowedMethods());

    return app;
}
