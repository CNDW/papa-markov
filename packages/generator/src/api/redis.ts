import Redis from 'ioredis';

function parseEnv(varname: string) {
    const val = process.env[varname];
    if (!val) {
        return '';
    }

    try {
        return JSON.parse(val);
    } catch (e) {
        return val;
    }
}

const redisConfig = {
    host: parseEnv('REDIS_HOST') || 'localhost',
    port: parseEnv('REDIS_PORT') || 6379
};

const redis = new Redis(redisConfig);

export {redis};
