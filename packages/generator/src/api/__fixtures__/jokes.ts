export const jokes = [
    {
        'id': 'zPRCljGQ7Ed',
        'joke': 'I have the heart of a lion... and a lifetime ban from the San Diego Zoo.',
        'status': 200
    },
    {
        'id': 'kOfaUvP7Muc',
        'joke': 'What did the Dorito farmer say to the other Dorito farmer? Cool Ranch!',
        'status': 200
    },
    {
        'id': '2wkykjyIYDd',
        'joke': 'What did the traffic light say to the car as it passed? "Don\'t look I\'m changing!"',
        'status': 200
    },
    {
        'id': 'FtHBXvXL6h',
        'joke': 'I was going to get a brain transplant, but I changed my mind',
        'status': 200
    },
    {
        'id': 'szItcaFQnjb',
        'joke': 'Why was the broom late for the meeting? He overswept.',
        'status': 200
    },
    {
        'id': 'HQfNZDIRSnb',
        'joke': 'A quick shoutout to all of the sidewalks out there... Thanks for keeping me off the streets.',
        'status': 200
    },
    {
        'id': 'VnysHtWvkqc',
        'joke': 'Geology rocks, but Geography is where it\'s at!',
        'status': 200
    },
    {
        'id': 'LB5wHQSvzAd',
        'joke': 'Where was the Declaration of Independence signed?\r\n\r\nAt the bottom! ',
        'status': 200
    },
    {
        'id': 'KJmrOKeNexc',
        'joke': 'This morning I was wondering where the sun was, but then it dawned on me.',
        'status': 200
    },
    {
        'id': 'pGe2EYozAsc',
        'joke': 'What do you call a nervous javelin thrower? Shakespeare.',
        'status': 200
    }
];
