const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    devServer: {
        contentBase: './static',
        historyApiFallback: true,
        writeToDisk: true,
        port: 3030
    },
    entry: {
        'app': path.resolve(__dirname, 'src', 'index.tsx')
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'static', 'build')
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.mjs', '.jsx', '.d.ts']
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'ts-loader' },
            { test: /\.scss$/, use: [ 'style-loader', 'css-loader', 'sass-loader' ] },
            { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] },
            { test: /\.(html|jpg|png|svg)$/, use: 'file-loader' }
        ]
    }
};
