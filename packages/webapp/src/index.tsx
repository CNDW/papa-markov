import React from 'react';
import { render } from 'react-dom';
import App from './app';

import '@blueprintjs/core/lib/css/blueprint.css';

const MOUNT_NODE = document.getElementById('app');

render( <App/>, MOUNT_NODE );
