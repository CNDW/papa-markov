import React from 'react';
import axios from 'axios';
import { H1, H2, H3, Card, Elevation, ButtonGroup, Button, Alignment, NumericInput } from '@blueprintjs/core';

import './app.scss';

export default class App extends React.Component<any, any> {
    public state = {
        badJokesCount: 0,
        goodJokesCount: 0,
        joke: '',
        jokesRead: 0,
        trainCount: 1
    }

    constructor(props: any) {
        super(props);
        this.fetchJoke();
    }

    fetchJoke = async () => {
        const { data } = await axios.get('http://localhost:3000/dad-joke');
        this.setState({ ...data });
    }

    sendBadJoke = async () => {
        const { joke } = this.state;
        const { data } = await axios.post('http://localhost:3000/dad-joke', { bad: joke });
        this.setState({ ...data });
    }

    sendGoodJoke = async () => {
        const { joke } = this.state;
        const { data } = await axios.post('http://localhost:3000/dad-joke', { good: joke });
        this.setState({ ...data });
    }

    trainMoar = async () => {
        const { trainCount } = this.state;
        const { data } = await axios.post('http://localhost:3000/train', { count: trainCount});
        this.setState({ ...data });
        this.fetchJoke();
    }

    reset = async () => {
        const { data } = await axios.post('http://localhost:3000/restart');
        this.setState({ ...data });
    }

    onTrainCountChange = async (value: number) => {
        this.setState({ trainCount: value });
    }

    render() {
        const { joke, jokesRead, trainCount, badJokesCount, goodJokesCount } = this.state;
        return (
            <div className="app-container">
                <H1>Papa Markov</H1>
                <H2 className="sub-head">Here to tell you all of the dad jokes</H2>
                <H3>Jokes Trained: {jokesRead}</H3>
                <H3>Bad Jokes Told: {badJokesCount}</H3>
                <H3>Good Jokes Told: {goodJokesCount}</H3>
                <ButtonGroup className="sub-head" alignText={Alignment.CENTER}>
                    <Button onClick={this.trainMoar} intent="primary" text='Train on more jokes' />
                    <NumericInput min={1} onValueChange={this.onTrainCountChange} value={trainCount} />
                </ButtonGroup>
                { joke ? (
                    <React.Fragment>
                        <Card elevation={Elevation.TWO}>
                            <p>
                                {joke}
                            </p>
                        </Card>

                        <ButtonGroup className="sub-head" alignText={Alignment.CENTER}>
                            <Button onClick={this.sendBadJoke} icon="refresh" intent="danger" text="Bad joke, try again" />
                            <Button onClick={this.sendGoodJoke} icon="refresh" intent="success" text="Ha Ha, good one dad..." />
                        </ButtonGroup>
                    </React.Fragment>
                ) : (
                    <Card elevation={Elevation.TWO}>
                        <p>
                            Dad doesn't know any jokes, train on more jokes and <Button intent="primary" text="ask him to tell you one" onClick={this.fetchJoke}/>
                        </p>
                    </Card>
                )}


                <ButtonGroup alignText={Alignment.CENTER}>
                    <Button onClick={this.reset} icon="refresh" intent="danger" text='Forget everything...' />
                </ButtonGroup>
            </div>
        );
    }
}
